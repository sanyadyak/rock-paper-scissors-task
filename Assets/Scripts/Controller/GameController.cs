using System;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
	[SerializeField] private List<PlayerControllerBase> _players;
	[SerializeField] private GameView _gameView;
	
	private GameModel _gameModel;
	private RulesModel _rulesModel;
	private AIModel _aiModel;

	public void StartGame(GameModel gameModel, RulesModel rulesModel, AIModel aiModel)
	{
		_gameModel = gameModel;
		_rulesModel = rulesModel;
		_aiModel = aiModel;

		for (int i = 0; i < _players.Count; i++)
		{
			var player = _players[i];
			player.SetId(i);
			
			var aiPlayer = player as AIPlayerController;
			if (aiPlayer != null)
			{
				aiPlayer.SetModel(_aiModel);
				aiPlayer.SetName("Computer");
			}
			else
			{
				player.SetName(i == 0 ? "First player" : "Second player");
			}
			
			player.StartGame();
		}

		NewRound();
	}

	private void NewRound()
	{
		var round = _gameModel.NewRound(_rulesModel);
		foreach (var player in _players)
		{
			player.StartRound(round);
		}

		round.RoundFinished += OnRoundFinished;
	}

	private void OnRoundFinished()
	{
		_gameModel.CurrentRound.RoundFinished -= OnRoundFinished;

		var winner = _gameModel.FinishRound();
		var resultStr = "";

		switch (winner)
		{
			case RoundResult.FirstPlayer:
				resultStr = $"{_players[0].Name} wins";
				break;
			
			case RoundResult.SecondPlayer:
				resultStr = $"{_players[1].Name} wins";
				break;
			
			case RoundResult.Draw:
				resultStr = "Draw!";	
				break;
			
			default:
				resultStr = "No winner";
				break;
		}


		_gameView.NewRoundPressed += OnNewRound;
		_gameView.ShowResult(resultStr, _gameModel.Score);
	}

	private void OnNewRound()
	{
		_gameView.NewRoundPressed -= OnNewRound;
		NewRound();
	}
}