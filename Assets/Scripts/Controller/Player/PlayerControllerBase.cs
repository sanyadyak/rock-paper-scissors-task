using UnityEngine;

public class PlayerControllerBase : MonoBehaviour
{
	protected int _id;
	//подумал, что нужно где-то хранить имя компа, но не уверен в этом сейчас
	private string _name;

	[SerializeField] protected GameView _view;

	protected RoundModel _roundModel;

	public string Name => _name;

	public virtual void StartGame()
	{
		
	}
	
	public virtual void StartRound(RoundModel roundModel)
	{
		_roundModel = roundModel;
	}

	protected virtual void OnShapeSelected(ShapeType shape)
	{
		
	}
	
	protected virtual void OnPlayerShapeSubmitted(int playerID, ShapeType shape)
	{
		
	}

	public void SetId(int id)
	{
		_id = id;
	}

	public void SetName(string name)
	{
		_name = name;
	}
}