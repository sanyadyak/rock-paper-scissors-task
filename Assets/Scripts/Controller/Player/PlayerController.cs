public class PlayerController : PlayerControllerBase
 {
 	public override void StartRound(RoundModel roundModel)
 	{
 		base.StartRound(roundModel);

		if (_id != roundModel.CurrentPlayerTurn)
		{
			_roundModel.PlayerShapeSubmitted += OnPlayerShapeSubmitted;
			return;
		}
		
		ShowShapeSelector();
 	}

	private void ShowShapeSelector()
	{
		_view.ShowInfo($"{Name} turn");
		_view.ShowShapesSelect(_roundModel.RulesModel.GetAllShapes());
        
		_view.ShapeSelected += OnShapeSelected;
	}
 
 	protected override void OnShapeSelected(ShapeType shape)
 	{
 		_view.ShapeSelected -= OnShapeSelected;
 		
 		base.OnShapeSelected(shape);
 		_roundModel.SetPlayerShape(_id, shape);
 	}

	protected override void OnPlayerShapeSubmitted(int playerID, ShapeType shape)
	{
		_roundModel.PlayerShapeSubmitted -= OnPlayerShapeSubmitted;
		
		ShowShapeSelector();
	}
 }