public class AIPlayerController : PlayerControllerBase
{
	private AIModel _aiModel;
	private ShapeType _selectedShape;

	public override void StartGame()
	{
		_view.HonestChanged += OnHonestChanged;
		_view.WinProbabilityChanged += OnWinProbabilityChanged;
		
		base.StartGame();
	}

	public void SetModel(AIModel model)
	{
		_aiModel = model;
		_view.ShowAISetup(_aiModel.Honest, _aiModel.WinProbability);
	}
	
	public override void StartRound(RoundModel roundModel)
	{
		if (_roundModel != null)
		{
			roundModel.PlayerShapeSubmitted -= OnPlayerShapeSubmitted;
		}
		
		base.StartRound(roundModel);
		roundModel.PlayerShapeSubmitted += OnPlayerShapeSubmitted;
	}

	protected override void OnPlayerShapeSubmitted(int playerID, ShapeType shape)
	{
		if (_id == playerID || _aiModel == null) return;

		_roundModel.PlayerShapeSubmitted -= OnPlayerShapeSubmitted;

		_selectedShape = _aiModel.GetShape(shape);
		_view.AiTurnShowed += OnTurnShowed;
		_view.ShowAIShapeSelect(Name, _selectedShape);
	}

	private void OnHonestChanged(bool honest)
	{
		if (_aiModel != null) _aiModel.Honest = honest;
	}

	private void OnWinProbabilityChanged(float value)
	{
		if (_aiModel != null) _aiModel.WinProbability = value;
	}

	private void OnTurnShowed()
	{
		_view.AiTurnShowed -= OnTurnShowed;
		_roundModel.SetPlayerShape(_id, _selectedShape);
	}
}