
public enum ShapeType
{
	None,
	Rock,
	Paper,
	Scissors
}