using System;

public class GameModel
{
	private int[] _score;
	
	public RoundModel CurrentRound { get; private set; }
	public int[] Score => _score;
	
	public GameModel()
	{
		_score = new int[2];
	}

	public RoundModel NewRound(RulesModel rulesModel)
	{
		CurrentRound = new RoundModel(rulesModel);
		return CurrentRound;
	}
	
	public RoundResult FinishRound()
	{
		if (CurrentRound == null || !CurrentRound.IsFinished) return RoundResult.None;
		
		switch (CurrentRound.Winner)
		{
			case RoundResult.FirstPlayer:
				_score[0]++;
				break;
			
			case RoundResult.SecondPlayer:
				_score[1]++;
				break;
		}

		return CurrentRound.Winner;
	}
}