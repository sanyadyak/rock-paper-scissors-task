using System;

public class RoundModel
{
	public event Action RoundFinished = delegate {};
	public event Action<int, ShapeType> PlayerShapeSubmitted = delegate {};

	private RulesModel _rulesModel;
	private ShapeType[] _selectedShapes;

	public RulesModel RulesModel => _rulesModel;
	public bool IsFinished { get; private set; }
	public RoundResult Winner { get; private set; }
	
	public int CurrentPlayerTurn => _selectedShapes[0] == ShapeType.None ? 0 : 1;

	public RoundModel(RulesModel rulesModel)
	{
		_selectedShapes = new[] {ShapeType.None, ShapeType.None};
		_rulesModel = rulesModel;
	}

	public void SetPlayerShape(int playerId, ShapeType shape)
	{
		if ((_rulesModel == null) ||
			(playerId < 0) || 
		    (playerId > 1) ||
			(shape == ShapeType.None) ||
			(playerId != CurrentPlayerTurn)) return;
		
		_selectedShapes[playerId] = shape;

		PlayerShapeSubmitted.Invoke(playerId, shape);

		if ((_selectedShapes[0] == ShapeType.None) || (_selectedShapes[1] == ShapeType.None)) return;

		IsFinished = true;
		Winner = _rulesModel.GetResult(_selectedShapes[0], _selectedShapes[1]);

		RoundFinished.Invoke();
	}
}