using System.Collections.Generic;
using System.Linq;

public class RulesModel
{
	static Dictionary<ShapeType, List<ShapeType>> _interactions = new Dictionary<ShapeType, List<ShapeType>>()
	{
		{ShapeType.Rock, new List<ShapeType> {ShapeType.Paper}},
		{ShapeType.Paper, new List<ShapeType> {ShapeType.Scissors}},
		{ShapeType.Scissors, new List<ShapeType> {ShapeType.Rock /*, ShapeType.Spoke*/}}
	};

	public List<ShapeType> GetAllShapes()
	{
		return new List<ShapeType>(_interactions.Keys);
	}

	public List<ShapeType> GetAllWinShapes(ShapeType toShape)
	{
		return new List<ShapeType>(_interactions[toShape]);
	}

	public List<ShapeType> GetAllLoseShapes(ShapeType toShape)
	{
		var loseShapes = new List<ShapeType>();
		
		foreach (var shape in _interactions.Keys)
		{
			if (GetResult(toShape, shape) == RoundResult.FirstPlayer) loseShapes.Add(shape);
		}

		return loseShapes;
	}

	public RoundResult GetResult(ShapeType firstShape, ShapeType secondShape)
	{
		if (firstShape == ShapeType.None || secondShape == ShapeType.None) return RoundResult.None;
		
		if (firstShape == secondShape) return RoundResult.Draw;
		
		if (_interactions[secondShape].Any(type => type == firstShape)) return RoundResult.FirstPlayer;

		if (_interactions[firstShape].Any(type => type == secondShape)) return RoundResult.SecondPlayer;

		return RoundResult.None;
	}
}