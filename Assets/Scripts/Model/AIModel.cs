using System.Collections.Generic;
using UnityEngine;

public class AIModel
{
	private RulesModel _rulesModel;
	private bool _honest;
	private float _winProbability;
	
	public bool Honest { get { return _honest; } set { _honest = value; } }
	
	public float WinProbability { get { return _winProbability;} set { _winProbability = Mathf.Clamp01(value); } }

	public AIModel(RulesModel rulesModel, bool honest, float winProbability = 0.5f)
	{
		SetRules(rulesModel);
		
		Honest = honest;
		WinProbability = winProbability;
	}

	public void SetRules(RulesModel rulesModel)
	{
		_rulesModel = rulesModel;
	}
	
	public ShapeType GetShape(ShapeType opponentShape)
	{
		List<ShapeType> possibleShapes;

		if (_honest)
		{
			possibleShapes = _rulesModel.GetAllShapes();
		}
		else
		{
			bool aiShouldWin = Random.value < _winProbability;

			possibleShapes = aiShouldWin
				? _rulesModel.GetAllWinShapes(opponentShape)
				: _rulesModel.GetAllLoseShapes(opponentShape);
		}

		return possibleShapes[Random.Range(0, possibleShapes.Count)];
	}
}