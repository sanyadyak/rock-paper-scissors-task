public enum RoundResult
{
	None,
	FirstPlayer,
	SecondPlayer,
	Draw
}