using UnityEngine;

public class GameRoot : MonoBehaviour
{
	[SerializeField] private GameController _gameController;
	
	private GameModel _gameModel;
	private RulesModel _rulesModel;
	private AIModel _aiModel;

	void Start()
	{
		_gameModel = new GameModel();
		_rulesModel = new RulesModel();
		_aiModel = new AIModel(_rulesModel, true);
		
		_gameController.StartGame(_gameModel, _rulesModel, _aiModel);
	}
}