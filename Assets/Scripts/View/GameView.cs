using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameView : MonoBehaviour
{
	[SerializeField] private Text _score, _info;
	[SerializeField] private GameObject _shapeButtonsContainer;
	[SerializeField] private Button _newRoundButton;
	[SerializeField] private Toggle _honestAi;
	[SerializeField] private Slider _aiWinProbability;
	[SerializeField] private Text _aiWinProbabilityValue;
	
	[SerializeField] private int _newScoreFontSize;

	private int _normalScoreFontSize;
	
	public event Action<ShapeType> ShapeSelected = delegate {};
	public event Action<bool> HonestChanged = delegate {};
	public event Action<float> WinProbabilityChanged = delegate {};
	public event Action AiTurnShowed = delegate {};
	public event Action NewRoundPressed = delegate {};

	private Button _buttonPrefab;
	private List<Button> _buttons = new List<Button>();
	
	void Awake()
	{
		_buttonPrefab = _shapeButtonsContainer.GetComponentInChildren<Button>(true);
		_buttons.Add(_buttonPrefab);
		
		_shapeButtonsContainer.SetActive(false);
		
		_honestAi.onValueChanged.RemoveAllListeners();
		_aiWinProbability.onValueChanged.RemoveAllListeners();

		_honestAi.onValueChanged.AddListener(isOne =>
		{
			_aiWinProbability.interactable = !isOne;
			HonestChanged.Invoke(isOne);
		});
		
		_aiWinProbability.onValueChanged.AddListener(value =>
		{
			_aiWinProbabilityValue.text = value.ToString("0.0");
			WinProbabilityChanged.Invoke(value);
		});
		
		_newRoundButton.onClick.RemoveAllListeners();
		_newRoundButton.onClick.AddListener(() => NewRoundPressed.Invoke());

		_normalScoreFontSize = _score.fontSize;
		ShowScore(0, 0);
	}

	void Update()
	{
		if (_newRoundButton.gameObject.activeInHierarchy && Input.GetMouseButtonDown(0))
		{
			_newRoundButton.onClick.Invoke();
		}
	}

	public void ShowScore(int left, int right)
	{
		_score.text = $"{left} : {right}";
	}

	public void ShowInfo(string text)
	{
		_info.text = text;
	}

	public void ShowShapesSelect(List<ShapeType> possibleShapes)
	{
		_shapeButtonsContainer.SetActive(true);
		
		_newRoundButton.gameObject.SetActive(false);

		for (int i = 0; i < possibleShapes.Count; i++)
		{
			Button button;
			
			if (i >= _buttons.Count)
			{
				button = Instantiate(_buttonPrefab, _buttonPrefab.transform.parent);
				button.transform.localPosition = _buttonPrefab.transform.localPosition + Vector3.down * (40 * i);
				
				_buttons.Add(button);
			}
			else
			{
				button = _buttons[i];
			}
			
			var shape = possibleShapes[i];
			
			button.gameObject.SetActive(true);
			button.GetComponentInChildren<Text>().text = shape.ToString();
			button.onClick.RemoveAllListeners();
			button.onClick.AddListener(() => ShapeSelected.Invoke(shape));
		}

		for (int i = possibleShapes.Count; i < _buttons.Count; i++)
		{
			_buttons[i].gameObject.SetActive(false);
		}
	}

	public void ShowAISetup(bool honest, float winProbability)
	{
		_honestAi.isOn = honest;
		
		_aiWinProbability.interactable = !honest;
		_aiWinProbability.value = winProbability;
	}

	public void ShowAIShapeSelect(string playerName, ShapeType shape)
	{
		StartCoroutine(AIShapeSelectCoroutine(playerName, shape));
	}
	
	IEnumerator AIShapeSelectCoroutine(string playerName, ShapeType shape)
	{
		_shapeButtonsContainer.SetActive(false);
		_newRoundButton.gameObject.SetActive(false);
		
		yield return new WaitForSeconds(0.3f);
		ShowInfo($"{playerName} turn:");
		
		yield return new WaitForSeconds(1.2f);
		ShowInfo($"{playerName} turn:\n {shape}");
		
		yield return new WaitForSeconds(1f);
		AiTurnShowed.Invoke();
	}

	public void ShowResult(string winner, int[] score)
	{
		_shapeButtonsContainer.SetActive(false);
		StartCoroutine(ResultCoroutine(winner, score));
	}   

	IEnumerator ResultCoroutine(string winner, int[] score)
	{
		yield return new WaitForSeconds(0.2f);
		ShowInfo(winner);
		
		yield return new WaitForSeconds(1);
		_score.fontSize = _newScoreFontSize;
		ShowScore(score[0], score[1]);
		
		yield return new WaitForSeconds(0.5f);
		_score.fontSize = _normalScoreFontSize;
		
		yield return new WaitForSeconds(0.2f);
		_newRoundButton.gameObject.SetActive(true);
	}
}