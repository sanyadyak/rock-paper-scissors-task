﻿using NUnit.Framework;

public class RoundModelTest
{

	[Test]
	public void Should_NotFinished_When_Null_Rules()
	{
		var round = new RoundModel(null);
		
		round.SetPlayerShape(0, ShapeType.Scissors);
		round.SetPlayerShape(-1, ShapeType.Paper);
		
		Assert.IsFalse(round.IsFinished);
	}

	[Test]
	public void Should_NotFinished_When_WrongId()
	{
		var round = new RoundModel(new RulesModel());
		
		round.SetPlayerShape(0, ShapeType.Scissors);
		round.SetPlayerShape(-1, ShapeType.Paper);
		
		Assert.IsFalse(round.IsFinished);
	}

	[Test]
	public void Should_NotFinished_When_NoneShape()
	{
		var round = new RoundModel(new RulesModel());
		
		round.SetPlayerShape(0, ShapeType.None);
		round.SetPlayerShape(1, ShapeType.Rock);
		
		Assert.IsFalse(round.IsFinished);
	}

	[Test]
	public void Should_NotFinished_When_SamePlayer()
	{
		var round = new RoundModel(new RulesModel());
		
		round.SetPlayerShape(0, ShapeType.Paper);
		round.SetPlayerShape(0, ShapeType.Rock);
		
		Assert.IsFalse(round.IsFinished);
	}

	[Test]
	public void Should_NotFinished_When_OneShape()
	{
		var round = new RoundModel(new RulesModel());
		
		round.SetPlayerShape(0, ShapeType.Scissors);
		
		Assert.IsFalse(round.IsFinished);
	}
	
	[Test]
	public void Should_NotFinished_When_NoShape()
	{
		var round = new RoundModel(new RulesModel());
		
		Assert.IsFalse(round.IsFinished);
	}
	
	[Test]
	public void Should_FirstWin_When_ScissorsBeatsPapper()
	{
		var round = new RoundModel(new RulesModel());
		
		round.SetPlayerShape(0, ShapeType.Scissors);
		round.SetPlayerShape(1, ShapeType.Paper);
		
		Assert.AreEqual(RoundResult.FirstPlayer, round.Winner);
	}
	
	[Test]
	public void Should_Draw_When_ScissorsAndScissors()
	{
		var round = new RoundModel(new RulesModel());
		
		round.SetPlayerShape(0, ShapeType.Scissors);
		round.SetPlayerShape(1, ShapeType.Scissors);
		
		Assert.AreEqual(RoundResult.Draw, round.Winner);
	}
}
